# React Native App

## Prerequisites

- NodeJS 14 or later
- npm 8 or later

## Getting started

npm install

[npx expo start](https://reactnative.dev/docs/environment-setup)

## Screenshots

Tested on web and IOS(iphone 11)

![screenshot 1](https://gitlab.com/m.mytiuk/MobileLabs/-/raw/main/assets/result/1.PNG)

![screenshot 2](https://gitlab.com/m.mytiuk/MobileLabs/-/raw/main/assets/result/2.PNG)

![screenshot 3](https://gitlab.com/m.mytiuk/MobileLabs/-/raw/main/assets/result/3.PNG)
