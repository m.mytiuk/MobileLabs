import { StyleSheet, SafeAreaView, View } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
const Stack = createNativeStackNavigator();

import { Routs } from "./utils/routing";
import Header from "./components/Header";
import Footer from "./components/Footer";
import Home from "./components/screens/Home";
import Gallery from "./components/screens/Gallery";
import Form from "./components/screens/Form";

export default function App() {
  return (
    <SafeAreaView style={styles.container}>
      <NavigationContainer>
        <View style={styles.container}>
          <Stack.Navigator
            initialRouteName={Routs.Home}
            screenOptions={{
              header: ({ navigation, route }) => (
                <Header
                  navigation={navigation}
                  currentRoute={route.name || Routs.Home}
                />
              ),
            }}
          >
            <Stack.Screen name={Routs.Home} component={Home} />
            <Stack.Screen name={Routs.Gallery} component={Gallery} />
            <Stack.Screen name={Routs.Form} component={Form} />
          </Stack.Navigator>

          <Footer />
        </View>
      </NavigationContainer>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#191919",
  },
});
