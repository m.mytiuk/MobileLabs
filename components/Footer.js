import { Text, View, StyleSheet } from "react-native";

export default function Footer() {
  return (
    <View style={styles.container}>
      <Text style={styles.text}>Митюк Максим Сергійович ЗІПЗкд-20-1</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    height: 30,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#191919",
  },
  text: {
    fontSize: 12,
    fontStyle: "italic",
    color: "#fff",
  },
});
