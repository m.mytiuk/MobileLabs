import { Image, Text, View, StyleSheet, TouchableOpacity } from "react-native";
import { Routs } from "../utils/routing";

export default function Menu({ navigation, currentRoute }) {
  const home = require(`../assets/images/home.png`);
  const home_active = require(`../assets/images/home_active.png`);
  const gallery = require(`../assets/images/gallery.png`);
  const gallery_active = require(`../assets/images/gallery_active.png`);
  const form = require(`../assets/images/form.png`);
  const form_active = require(`../assets/images/form_active.png`);

  return (
    <View style={styles.menu}>
      <MenuItem
        name={Routs.Home}
        img={home}
        imgActive={home_active}
        navigation={navigation}
        currentRoute={currentRoute}
      />
      <MenuItem
        name={Routs.Gallery}
        img={gallery}
        imgActive={gallery_active}
        navigation={navigation}
        currentRoute={currentRoute}
      />
      <MenuItem
        name={Routs.Form}
        img={form}
        imgActive={form_active}
        navigation={navigation}
        currentRoute={currentRoute}
      />
    </View>
  );
}

function MenuItem({ name, img, imgActive, navigation, currentRoute }) {
  let textStyle = currentRoute === name ? styles.text_active : styles.text;
  let imgSource = currentRoute === name ? imgActive : img;

  return (
    <TouchableOpacity onPress={() => navigation.navigate(name)}>
      <View style={styles.menu_item} onClick={() => navigation.navigate(name)}>
        <Image style={styles.logo} source={imgSource} />
        <Text style={textStyle}>{name}</Text>
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  menu: {
    height: 56,
    flexDirection: "row",
    justifyContent: "space-evenly",
    alignItems: "center",
    backgroundColor: "#dedede",
  },
  menu_item: {
    alignItems: "center",
  },
  logo: {
    width: 24,
    height: 24,
  },
  text: {
    marginTop: 4,
    fontSize: 10,
    fontWeight: "bold",
  },
  text_active: {
    marginTop: 4,
    fontSize: 10,
    fontWeight: "bold",
    color: "#0363b0",
  },
});
