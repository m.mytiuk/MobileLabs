import { Text, ScrollView, View, StyleSheet, Image } from "react-native";

export default function Home() {
  return (
    <View style={styles.container}>
      <Text style={styles.title}>News</Text>

      <ScrollView>
        {Array.from({ length: 100 }).map((_, index) => (
          <NewsItem key={index} index={index} />
        ))}
      </ScrollView>
    </View>
  );
}

function NewsItem({ index }) {
  return (
    <View style={styles.news_container}>
      <Image
        style={styles.image}
        source={require("../../assets/images/react.png")}
      />
      <View style={styles.news_info}>
        <Text style={styles.info_title}>Title {index + 1}</Text>
        <Text style={styles.info_caption}>
          Lorem Ipsum is simply dummy text...
        </Text>
        <Text style={styles.info_data}>{new Date().toJSON().slice(0, 10)}</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingLeft: 16,
  },
  news_container: {
    flexDirection: "row",
  },
  news_info: {
    marginLeft: 15,
  },
  title: {
    marginTop: 10,
    marginBottom: 10,
    fontSize: 18,
    fontWeight: "bold",
    textAlign: "center",
  },
  image: {
    marginBottom: 10,
    width: 70,
    height: 70,
    resizeMode: "contain",
  },
  info_title: {
    fontSize: 16,
    fontWeight: "bold",
  },
  info_caption: {
    marginTop: 10,
    fontSize: 12,
    fontWeight: "bold",
  },
  info_data: {
    fontSize: 8,
    marginTop: 15,
  },
});
