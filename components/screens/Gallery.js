import { ScrollView, View, StyleSheet, Image } from "react-native";

export default function Gallery() {
  return (
    <View style={styles.container}>
      <ScrollView>
        <View style={styles.item_wrapper}>
          {Array.from({ length: 24 }).map((_, index) => (
            <View key={index} style={styles.image_wrapper}></View>
          ))}
        </View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  item_wrapper: {
    flexDirection: "row",
    flexWrap: "wrap",
    justifyContent: "space-evenly",
    marginTop: 5,
  },
  image_wrapper: {
    marginVertical: 5,
    width: "45%",
    height: 160,

    shadowOffset: { width: 1, height: 2 },
    shadowColor: "#000",
    shadowOpacity: 0.3,
    shadowRadius: 6,
    backgroundColor: "#fff",
  },
});
