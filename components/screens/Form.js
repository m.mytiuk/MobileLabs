import { useState } from "react";
import {
  Alert,
  Button,
  Text,
  ScrollView,
  View,
  StyleSheet,
  TextInput,
} from "react-native";

export default function Form() {
  const [email, onChangeEmail] = useState("");
  const [password, onChangePassword] = useState("");
  const [passwordAgain, onChangePasswordAgain] = useState("");
  const [secondName, onChangeSecondName] = useState("");
  const [name, onChangeName] = useState("");

  function sendForm() {
    Alert.alert(`User ${email} succesfully registred`);
  }

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Sign up</Text>

      <ScrollView>
        <Text style={styles.placeholder}>Email</Text>
        <TextInput
          style={styles.input}
          onChangeText={onChangeEmail}
          value={email}
        />

        <Text style={styles.placeholder}>Password</Text>
        <TextInput
          style={styles.input}
          value={password}
          secureTextEntry={true}
          onChangeText={onChangePassword}
        />

        <Text style={styles.placeholder}>Password (again)</Text>
        <TextInput
          style={[styles.input, { marginBottom: 50 }]}
          value={passwordAgain}
          secureTextEntry={true}
          onChangeText={onChangePasswordAgain}
        />

        <Text style={styles.placeholder}>Second Name</Text>
        <TextInput
          style={styles.input}
          onChangeText={onChangeSecondName}
          value={secondName}
        />

        <Text style={styles.placeholder}>Name</Text>
        <TextInput
          style={styles.input}
          onChangeText={onChangeName}
          value={name}
        />

        <Button title="Sign up" onPress={() => sendForm} />
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 16,
  },
  title: {
    marginTop: 10,
    marginBottom: 40,
    fontSize: 18,
    fontWeight: "bold",
    textAlign: "center",
  },
  placeholder: {
    marginBottom: 5,
    fontSize: 11,
  },
  input: {
    marginBottom: 20,
    height: 40,
    padding: 10,
    borderWidth: 1,
  },
});
