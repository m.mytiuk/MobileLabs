import { Image, Text, View, StyleSheet } from "react-native";

import Menu from "./Menu";

export default function Header({ navigation, currentRoute }) {
  return (
    <>
      <View style={styles.header}>
        <Image
          style={styles.logo}
          source={require("../assets/images/logo.png")}
        />
        <Text style={styles.title}>MyFirstApp</Text>
      </View>

      <Menu navigation={navigation} currentRoute={currentRoute} />
    </>
  );
}
const styles = StyleSheet.create({
  header: {
    height: 40,
    paddingLeft: 10,
    paddingRight: 50,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    backgroundColor: "#191919",
  },
  title: {
    fontSize: 18,
    fontWeight: "bold",
    color: "#fff",
  },
  logo: {
    width: 100,
    height: 30,
  },
});
